from arista import Arista
from nodo import Nodo


def reconstruir_camino(actual, padre):
    camino = []
    while actual in padre.keys():
        camino.append(actual)
        actual = padre[actual]
    camino.append(actual)
    for nodo in camino:
        print(nodo.dato)
    return camino


def h(actual, objetivo):
    return ord(objetivo.dato) - ord(actual.dato)


def algoritmo_a(inicio, fin):
    nodos_abiertos = [inicio]   # al principio el único nodo abierto es el de inicio
    nodos_cerrados = []
    g = {
        inicio: 0   # no se ha avanzado nada
    }
    f = {
        inicio: h(inicio, fin)  # se calcula el valor heurístico inicial
    }
    padre = {}  # no se han recorrido caminos

    while len(nodos_abiertos) > 0:
        actual = nodos_abiertos.pop(0)  # se toma el primer elemento de la pila
        nodos_cerrados.append(actual)
        if actual == fin:   # cuando se ha terminado
            return reconstruir_camino(actual, padre)    # se imprime el camino
        for arista in actual.aristas:
            g_posible = g[actual] + arista.peso     # se le suma el peso de la arista actual
            # si el vecino no se ha recorrido, o bien este camino es más óptimo
            if arista.vecino not in nodos_cerrados or g_posible < g[arista.vecino]:
                g[arista.vecino] = g_posible
                f[arista.vecino] = g_posible + h(arista.vecino, fin)    # f = g + h
                padre[arista.vecino] = actual
                if arista.vecino not in nodos_abiertos:
                    for i in range(len(nodos_abiertos)):    # se inserta el nodo, ordenado de acuerdo al valor de f
                        if f[nodos_abiertos[i]] > f[arista.vecino]:
                            nodos_abiertos.insert(i, arista.vecino)
                            break
                    else:
                        nodos_abiertos.append(arista.vecino)


class Grafo:
    def __init__(self):
        self.V = {}

    def agregar_arista(self, uno, otro, peso):
        self.V[uno].agregar_vecino(Arista(self.V[otro], peso))
        self.V[otro].agregar_vecino(Arista(self.V[uno], peso))

    def ejemplo1(self):
        self.V = {
            'A': Nodo('A'),
            'B': Nodo('B'),
            'C': Nodo('C'),
            'D': Nodo('D')
        }
        self.agregar_arista('A', 'B', 2)
        self.agregar_arista('A', 'C', 1)
        self.agregar_arista('B', 'D', 3)
        self.agregar_arista('C', 'D', 2)
