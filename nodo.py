class Nodo:
    def __init__(self, dato=None):
        self.dato = dato
        self.padre = None
        self.aristas = []
        self.costo = None

    def agregar_vecino(self, nodo):
        self.aristas.append(nodo)

    def b_profundidad(self, numero, profundidad_max, pila_nodos_abiertos, contador_profundidad=0):
        pila_nodos_abiertos.append(self)

        print("Nodo actual: " + str(self.dato))
        print("Profundidad: " + str(contador_profundidad))
        print("Nodos abiertos:", end=" ")
        for nodo in pila_nodos_abiertos:
            print(nodo.vecino, end=" ")
        print()

        if self.dato == numero:    # se llegó al destino
            return True

        contador_profundidad += 1
        if contador_profundidad <= profundidad_max:
            for hijo in self.aristas:
                if hijo.b_profundidad(numero, profundidad_max, pila_nodos_abiertos, contador_profundidad):
                    return True

        pila_nodos_abiertos.pop()
        print("Nodos abiertos:", end=" ")
        for nodo in pila_nodos_abiertos:
            print(nodo.vecino, end=" ")
        print()

        return False

    def b_profundidad_iterativa(self, numero, profundidad_max, contador_profundidad=-1):
        contador_profundidad += 1

        if contador_profundidad <= profundidad_max:
            print("\nIteración " + str(contador_profundidad))
            if self.b_profundidad(numero, contador_profundidad,
                                  []) or self.b_profundidad_iterativa(numero, profundidad_max, contador_profundidad):
                return True

        return False

    def algoritmo_voraz(self, numero_buscado, costo_total, pila_nodos_abiertos):
        if self.dato is None:
            return False

        print("Nodo actual: " + str(self.dato) + "\t Costo total: " + str(costo_total))

        if self.dato == numero_buscado:
            print("Se encontró el número " + str(self.dato) + " y costó " + str(costo_total))
            return True

        self.generar_sucesores_voraz(pila_nodos_abiertos)

        if pila_nodos_abiertos:
            siguiente_nodo = pila_nodos_abiertos.pop()
            costo_total += siguiente_nodo.costo
            return siguiente_nodo.algoritmo_voraz(numero_buscado, costo_total, pila_nodos_abiertos)

        print("La búsqueda terminó")

        return False

    def generar_sucesores_voraz(self, pila_nodos_abiertos):
        for i in range(len(self.aristas)):
            j_mayor = i
            for j in range(i + 1, len(self.aristas)):
                if self.aristas[j_mayor].costo < self.aristas[j].costo:
                    j_mayor = j
            self.aristas[i], self.aristas[j_mayor] = self.aristas[j_mayor], self.aristas[i]
            pila_nodos_abiertos.append(self.aristas[i])
