import random

from grafo import Grafo, algoritmo_a
from nodo import Nodo


def grafo():
    padre = Nodo('S')
    padre.agregar_vecino(Nodo('G'))
    padre.agregar_vecino(Nodo('R'))
    padre.agregar_vecino(Nodo('E'))
    padre.aristas[0].agregar_vecino(Nodo('O'))
    padre.aristas[0].aristas[0].agregar_vecino(Nodo('A'))
    padre.aristas[0].aristas[0].aristas[0].agregar_vecino(Nodo('N'))
    padre.aristas[2].agregar_vecino(Nodo('I'))
    padre.aristas[2].aristas[0].agregar_vecino(padre.aristas[0].aristas[0].aristas[0])
    padre.aristas[2].aristas[0].agregar_vecino(padre.aristas[0].aristas[0].aristas[0].aristas[0])
    return padre


def ejemplo_b_profundidad_iterativa():
    p = grafo()

    letra_buscar = input('Introduzca una de las siguientes letras (letras del grafo: S E R G I O A N): ').upper()
    print("Número a buscar en profundidad iterativa " + str(letra_buscar))

    if p.b_profundidad_iterativa(letra_buscar, 4):
        print("\nSe encontró " + letra_buscar)
    else:
        print("\nNo se encontró " + letra_buscar)


def crear_arbol_ejemplo(vec_num, indice_vec, longitud_vec, nodo_actual=None, nivel=0, nivel_max=4):
    if indice_vec >= longitud_vec:
        return
    nuevo_nodo = Nodo(vec_num[indice_vec])
    nuevo_nodo.costo = random.randint(1, 10)

    if nodo_actual is None:
        nodo_actual = nuevo_nodo
        crear_arbol_ejemplo(vec_num, indice_vec + 1, longitud_vec, nodo_actual, nivel + 1, nivel_max)
        return nodo_actual

    print("Índice: " + str(indice_vec) + ", raíz: " + str(nodo_actual.numero), "hijo: " + str(nuevo_nodo.dato))

    if nodo_actual.padre is None and len(nodo_actual.hijos) == 3:
        print("Se detiene antes de " + vec_num[indice_vec])
        return nodo_actual
    nuevo_nodo.padre = nodo_actual

    if nivel > nivel_max or len(nodo_actual.hijos) == 3:
        crear_arbol_ejemplo(vec_num, indice_vec, longitud_vec, nodo_actual.padre, nivel - 1, nivel_max)
        return nodo_actual

    if len(nodo_actual.hijos) == 0:
        print("Rama izquierda")
    if len(nodo_actual.hijos) == 1:
        print("Rama de en medio")
    if len(nodo_actual.hijos) == 1:
        print("Rama derecha")
    nodo_actual.hijos.append(nuevo_nodo)
    crear_arbol_ejemplo(vec_num, indice_vec + 1, longitud_vec, nuevo_nodo, nivel + 1, nivel_max)
    return nodo_actual


def ejemplo_algoritmo_voraz():
    lista = []
    for i in range(1, 101):
        lista.append(i)

    grafo_arbol = crear_arbol_ejemplo(lista, 0, 100)

    print("I: " + str(grafo_arbol.aristas[0].vecino) + ", costo: " + str(grafo_arbol.aristas[0].costo))
    print("M: " + str(grafo_arbol.aristas[1].vecino) + ", costo: " + str(grafo_arbol.aristas[1].costo))
    print("D: " + str(grafo_arbol.aristas[2].vecino) + ", costo: " + str(grafo_arbol.aristas[2].costo))

    grafo_arbol.algoritmo_voraz(42, 0, [])

    print("Algoritmo voraz, primero el mejor")


def ejemplo_a():
    g = Grafo()
    g.ejemplo1()
    algoritmo_a(g.V['A'], g.V['D'])


sub = int(input("Introduzca la subrutina a ejecutar:\n1. Búsqueda en profundidad iterativa\n2. Algoritmo voraz\n"
                "3. A*\n"))
if sub == 1:
    ejemplo_b_profundidad_iterativa()
elif sub == 2:
    ejemplo_algoritmo_voraz()
else:
    ejemplo_a()
